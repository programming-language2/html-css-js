let currentStep = 0;

function showStep(stepIndex) {
  const steps = document.querySelectorAll('.step');
  steps[currentStep].classList.remove('show');
  steps[currentStep].classList.add('transitioning');
  currentStep = stepIndex;
  steps[currentStep].classList.add('show');
  setTimeout(() => {
    steps[currentStep].classList.remove('transitioning');
  }, 300);

  const stepButtons = document.querySelectorAll('.step-btn');
  stepButtons.forEach((button, index) => {
    button.classList.remove('active');
    if (index === currentStep) {
      button.classList.add('active');
    }
  });
}

function nextStep(stepIndex) {
  showStep(stepIndex);
}

function prevStep(stepIndex) {
  showStep(stepIndex);
}
