let slideIndex = 0;
const slideInterval = 3000; // Change slide every 3 seconds

const slides = document.querySelectorAll('.slider img');
const totalSlides = slides.length;

function showSlide(index) {
  if (index < 0) {
    slideIndex = totalSlides - 1;
  } else if (index >= totalSlides) {
    slideIndex = 0;
  }

  const offset = -slideIndex * 100;
  document.querySelector('.slider').style.transform = `translateX(${offset}%)`;
}

function nextSlide() {
  slideIndex++;
  showSlide(slideIndex);
}

function previousSlide() {
  slideIndex--;
  showSlide(slideIndex);
}

setInterval(nextSlide, slideInterval);

// Optional: Pause the slider when hovering over it
const sliderContainer = document.querySelector('.slider-container');
sliderContainer.addEventListener('mouseenter', () => clearInterval(interval));
sliderContainer.addEventListener('mouseleave', () => setInterval(nextSlide, slideInterval));
